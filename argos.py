#!/usr/bin/env python3

import os
import sys
import csv
import subprocess
import requests
import xlsxwriter
try:
    from ConfigParser import ConfigParser, Error, NoOptionError
except:
    from configparser import ConfigParser, Error, NoOptionError

class Conf:
    def __init__(self, filename):
        self._filename = filename
        try:
            self._config = ConfigParser(strict=False)
        except:
            self._config = ConfigParser()
        try:
            self._config.read(os.path.expanduser(filename))
        except Exception as e:
            logging.error("[Conf]" + self._filename + ": " + str(e))
            raise Exception("Error during loading file " + self._filename)

    def getSection(self, section):
        data={}
        try:
            if section in self._config.sections():
                for name, value in self._config.items(section):
                    data[name] = value
        except Exception as e:
            logging.error("[Conf]" + self._filename + ": " + str(e))
        for key, value in data.items():
            if ", " in value:
                data[key] = value.split(", ")
        return data

    def get(self, section, option, default=""):
        val = default
        try:
            val = self._config.get(section, option)
        except:
            val = default
        if ", " in val:
            return val.split(", ")
        return default

    def sections(self):
        return self._config.sections()

    def setSection(self, section, values):
        if not self._config.has_section(section):
            self._config.add_section(section)
        for k, v in values.items():
            self._config.set(section, k, v)

    def setValue(self, section, option, value):
        if not self._config.has_section(section):
            self._config.add_section(section)
        self._config.set(section, option, value)

    def removeSection(self, section):
        if self._config.has_section(section):
            self._config.remove_section(section)

    def removeValue(self, section, option):
        if self._config.has_section(section) and self._config.has_option(section, option):
            self._config.remove_option(section, option)

    def save(self):
        with open(self._filename, 'w') as f:
            self._config.write(f)

    def getAll(self):
        data = {}
        for section in self.sections():
            data[section] = self.getSection(section)
        return data

def get_grades(registered):
    grades = {}
    for student in registered:
        grades[student['login']] = student['grade']
    return grades

def average(students):
    nb = 0
    for student in students:
        nb += students[student];
    return nb / float(len(students));

argos = Conf(os.path.expanduser("config" if os.path.exists("config") else "~/config")).getSection("argos")
intra= Conf(os.path.expanduser("config" if os.path.exists("config") else "~/config")).getSection("intra")

year = 2018
API_URI = argos['uri']
user = argos['user']
password = argos['password']

INTRA_URI = intra['uri']

if len(sys.argv) < 2:
    print("Usage: {} project_list.csv".format(sys.argv[0]), file=sys.stderr)
    sys.exit(1)

projects = []

try:
    with open(sys.argv[1], newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quotechar='"') 
        for row in reader:
            projects.append(row)
except Exception as e:
    print("Cannot open file {}:".format(sys.argv[1]), e, file=sys.stderr)
    sys.exit(1)

students = {}

workbook = xlsxwriter.Workbook('bttf_results.xlsx')
for project in projects:
    project_data = {'projectSlug': project['slug'], 'testRunType': 'bttf'}
    response = {}
    try:
        r = requests.post(API_URI + '/overview/{}/{}'.format(year, project['codemodule']), json=project_data, auth=(user, password), timeout=5)
        r.raise_for_status()
        response = r.json()
    except Exception as e:
        print("Failed to request argos: ", e, file=sys.stderr)
        workbook.close()
        sys.exit(1)

    if 'instances' not in response:
        workbook.close()
        sys.exit(1)

    students_grade = {}
    try:
        session = requests.Session()
        r = session.get(INTRA_URI + '/auth-{}'.format(intra['autologin']), timeout=5)
        r.raise_for_status()
        r = session.get(INTRA_URI + '/module/{}/{}/registered?format=json'.format(year, project['codemodule']), timeout=5)
        r.raise_for_status()
        students_grade = get_grades(r.json());
    except Exception as e:
        print("Failed to request argos: ", e, file=sys.stderr)
        workbook.close()
        sys.exit(1)
        
    worksheet = workbook.add_worksheet(project['codemodule'] + "_" + project['slug'])
    worksheet.write('A1', 'login')
    worksheet.write('B1', 'city')
    worksheet.write('C1', 'instance')
    worksheet.write('D1', 'grade')
    row = 1
    col = 0
    nb_results = 0
    for instance in response['instances']:
        city = instance['instance']['city']
        instance_code = instance['instance']['code']
        for result in instance['results']:
            for login in result['logins']:
                login = "{}@epitech.eu".format(login)
                if login not in students_grade or (students_grade[login] != 'Echec' and students_grade[login] != '-'):
                    continue
                if login not in students:
                    students[login] = 1
                else:
                    students[login] += 1
                nb_results += 1
                worksheet.write_string(row, col, login)
                worksheet.write_string(row, col + 1, city)
                worksheet.write_string(row, col + 2, instance_code)
                worksheet.write_string(row, col + 3, students_grade[login])
                row += 1
    print("[{0}]: {1}/{2} participated in BTTF, {3:.2f}%".format(project['codemodule'], nb_results, len(students_grade), nb_results/float(len(students_grade))*100))
print("{0} students participated for an average of {1:.2f} projects/student".format(len(students), average(students)))
workbook.close()
