#!/usr/bin/env python3

import sys
import csv
import subprocess

year = 2018

if len(sys.argv) < 2:
    print("Usage: {} project_list.csv".format(sys.argv[0]), file=sys.stderr)
    sys.exit(1)

projects = []

try:
    with open(sys.argv[1], newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';', quotechar='"')
        for row in reader:
            projects.append(row)
except Exception as e:
    print("Cannot open file {}:".format(sys.argv[1]), e, file=sys.stderr)
    sys.exit(1)

for project in projects:
    try:
        args = "pickup {} -Y {} -M {} --yes".format(project['slug'], year, project['codemodule'])
        retcode = subprocess.call("python3 ./Atom.py " + args, shell=True)
        if retcode < 0:
            print("Child was terminated by signal", -retcode, file=sys.stderr)
    except OSError as e:
        print("Execution failed:", e, file=sys.stderr)
